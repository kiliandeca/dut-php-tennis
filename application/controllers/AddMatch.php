<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 19/01/2018
 * Time: 01:00
 */

class AddMatch extends CI_Controller
{

    public function index(){
        if(!isset($this->session->rank)) {
            redirect('./login', 'refresh');
        }elseif ($this->session->rank<1) {
            redirect('./', 'refresh');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->model("Match_model", "", TRUE);

        $this->form_validation->set_rules('label', 'Label', 'required|is_unique[CPOA_MATCH_VENTE.label]');
        $this->form_validation->set_rules('prix', 'Prix', 'required');

        if ($this->form_validation->run() == true) {

            $match = new Match_model();
            $match->id = $this->input->post("id");
            $match->label = $this->input->post("label");
            $match->prix_base = $this->input->post("prix");
            $match->insert();

        }
        $matchs = $this->Match_model->getNotInSale();
        $data = array("matchs" => $matchs);
        $this->load->view('templates/template', array('view' => 'admin/add_match', 'data' => $data));

    }

}