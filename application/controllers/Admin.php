<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 19:55
 */

class Admin extends CI_Controller
{

    public function index(){

        if(!isset($this->session->rank)) {
            redirect('./login', 'refresh');
        }elseif ($this->session->rank<1) {
            redirect('./', 'refresh');
        }


        $this->load->view('templates/template', array('view'=>'admin/admin'));


    }

}