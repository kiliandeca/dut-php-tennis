<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 22:24
 */

class Courts extends CI_Controller
{
    public function index(){
        if(!isset($this->session->rank)) {
            redirect('./login', 'refresh');
        }elseif ($this->session->rank<1) {
            redirect('./', 'refresh');
        }

        $selectedCourt = $this->input->post('court');
        if(isset($selectedCourt)){
            redirect('/admin/courts/'.$selectedCourt, 'refresh');
        }


        $this->load->helper('form');
        $this->load->model("Court_model", "", TRUE);

        $courts = $this->Court_model->getCourts();

        $data = array("courts"=>$courts);


        $this->load->view('templates/template', array('view'=>'admin/courts', 'data'=>$data));

    }

    public function edit(){
        if(!isset($this->session->rank)) {
            redirect('./login', 'refresh');
        }elseif ($this->session->rank<0) {
            redirect('./', 'refresh');
        }


        $this->load->helper('form');
        $this->load->model("Capacite_model", "", TRUE);
        $this->load->model("Court_model", "", TRUE);

        if (isset($_POST["save"])){
            $capacite = new Capacite_model();
            $capacite->court = $this->uri->segment(3);
            $capacite->block = $this->input->post('block');
            $capacite->capacite = $this->input->post('capacite');
            $capacite->categorie = $this->input->post('categorie');
            $capacite->insert();
        }

        if(isset($_POST["delete"]))$this->Capacite_model->deleteById($this->input->post("delete"));

        if(isset($_POST["update"])){
            $capacite = new Capacite_model();
            $capacite->court = $this->uri->segment(3);
            $capacite->block = $this->input->post('block');
            $capacite->capacite = $this->input->post('capacite');
            $capacite->categorie = $this->input->post('categorie');
            $capacite->id = $this->input->post('id');
            $capacite->update();
        }


        $blocks = $this->Capacite_model->getByCourtId($this->uri->segment(3));
        $court = $this->Court_model->getById($this->uri->segment(3));
        $data = array("blocks"=>$blocks, "court"=>$court);


        $this->load->view('templates/template', array('view'=>'admin/edit_court', 'data'=>$data));
    }

}