<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


    public function index()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if(isset($this->session->logged)){
            redirect('./', 'refresh');
        }

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|callback_check');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/template', array('view'=>'login'));
        }
        else
        {
            //Success
            //Recup nombres de places dans le panier pour affichage dans le menu
            $this->load->model('Cart_model', '', TRUE);
            $this->session->set_userdata("cart", $this->Cart_model->getCountByUserId($this->session->id));

            $this->load->view('templates/template', array('view'=>'loginsuccess'));
        }
    }

    public function check($str)
    {
        $this->load->model('User_model', '', TRUE);
        $user = $this->User_model->get_by_email($this->input->post('email'));

        if($user and password_verify($str, $user->password)){


            $this->session->set_userdata(array("logged" => true,"id" => $user->id, "email" => $user->email, "rank" => $user->rank));
            return true;

        }

        $this->form_validation->set_message('check', "Ce compte n'existe pas ou le mot de passe est incorrect");
        return false;

    }

    public function logout(){
        session_destroy();
        redirect('./', 'refresh');
    }

}
