<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/01/2018
 * Time: 23:59
 */

class Match extends CI_Controller
{
    public function index($arg){
        $this->load->model('Match_model', '', TRUE);

        $match = $this->Match_model->getById($arg);

        if($match==null)redirect('./matchs', 'refresh');

        if($this->input->post("add")){
            if(!isset($this->session->logged)) {
                redirect('./login', 'refresh');
            }

            $this->load->model('Cart_model', '', TRUE);

            $cart = new Cart_model;
            $cart->user = $this->session->id;
            $cart->place = $this->input->post("id");

            $cart->insert();

            $this->session->set_userdata("cart", $this->Cart_model->getCountByUserId($this->session->id));


        }

        $data = array();
        $data["match"] = $match;
        $data["place"] = null;
        $data["blocks"] = null;
        $data["place"] = null;

        $this->load->model('Place_model', '', TRUE);
        $data["blocks"] = $this->Place_model->getBlocksLibres($arg);


        if($this->input->get("block")!=null) {
            $data["numeros"] = $this->Place_model->getPlacesLibresByBlock($arg, $_GET["block"]);

            if($this->input->get("num")!=null){
                $data["place"] = ($this->Place_model->getPlace($arg,$this->input->get("block"),$this->input->get("num")))[0];
            }


        }

        $this->load->view('templates/template', array('view'=> 'match', 'data'=>$data));

    }


}