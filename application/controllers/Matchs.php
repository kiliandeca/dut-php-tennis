<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/01/2018
 * Time: 23:59
 */

class Matchs extends CI_Controller
{
    public function index(){

        $this->load->model('Match_model', '', TRUE);

        $matchs = $this->Match_model->get_matches();

        $this->load->view('templates/template', array('view'=>'listeMatchs', 'data'=>array("matchs" =>$matchs)));

    }


}