<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 19/01/2018
 * Time: 16:00
 */

class Order extends CI_Controller
{

    public function index(){

        $data = array();
        $this->load->model('Order_model', '', TRUE);

        $data["orders"] = $this->Order_model->getByUser($this->session->id);

        $this->load->view('templates/template', array('view'=> 'orders', "data"=>$data));
    }

    public function orderpage($arg){
        $this->load->model('Order_model', '', TRUE);
        $order = $this->Order_model->getById($arg);
        if($order->iduser != $this->session->id){
            redirect('./', 'refresh');
        }


        $data = array();
        $this->load->model('Place_model', '', TRUE);

        $data["places"] = $this->Place_model->getByOrder($arg);

        $this->load->view('templates/template', array('view'=> 'order', "data"=>$data));


    }


}