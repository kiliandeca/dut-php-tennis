<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 19/01/2018
 * Time: 14:58
 */

class Panier extends CI_Controller
{

    public function index(){
        if(!isset($this->session->logged)) {
            redirect('./login', 'refresh');
        }

        $this->load->model('Cart_model', '', TRUE);

        if($this->input->post("delete")){
            $this->Cart_model->removeByPlaceAndUserId($this->session->id, $this->input->post("delete"));
            $this->session->set_userdata("cart", $this->Cart_model->getCountByUserId($this->session->id));
        }

        $data = array();

        if($this->input->post("promo")){
            $this->load->model('Reduc_model', '', TRUE);
            $promo = $this->Reduc_model->getByCode($this->input->post("promo"));
            if($promo != null and $promo[0]->restant>0) $data["promo"] = $promo[0];
        }

        $places = $this->Cart_model->getByUserId($this->session->id);

        $prixTotal = $this->Cart_model->getTotalPriceByUserId($this->session->id);

        $data["places"] = $places;
        $data["prixTotal"] = $prixTotal;

        $this->load->view('templates/template', array('view'=>'panier', "data"=>$data));

    }

    public function paiment(){
        if(!isset($this->session->logged)) {
            redirect('./login', 'refresh');
        }

        $this->load->model('Order_model', '', TRUE);
        $this->load->model('Cart_model', '', TRUE);


        $order =new Order_model();
        $order->iduser = $this->session->id;
        $order->prix_total = $this->Cart_model->getTotalPriceByUserId($order->iduser);
        if($this->input->post("promo")){
            $this->load->model('Reduc_model', '', TRUE);
            $promo = ($this->Reduc_model->getByCode($this->input->post("promo")))[0];
            $promo->useCode();
            $order->prix_total = $order->prix_total - number_format($order->prix_total*($promo->pourcentage/100),2);
        }
        $order->insert();

        $this->Cart_model->removeByUserId($order->iduser, $order->id);
        $this->session->set_userdata("cart", 0);

        redirect('./orders/'.$order->id, 'refresh');




    }

}