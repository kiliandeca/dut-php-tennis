<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 22/01/2018
 * Time: 22:51
 */

class Promo extends CI_Controller
{

    public function index(){
        if(!isset($this->session->rank)) {
            redirect('./login', 'refresh');
        }elseif ($this->session->rank<1) {
            redirect('./', 'refresh');
        }
        $this->load->model('Reduc_model', '', TRUE);

        if (isset($_POST["save"])){
            $reduc = new Reduc_model();
            $reduc->code = $this->input->post('code');
            $reduc->pourcentage = $this->input->post('pourcentage');
            $reduc->restant = $this->input->post('restant');
            $reduc->insert();
        }

        if(isset($_POST["delete"]))$this->Reduc_model->deleteById($this->input->post("delete"));


        if(isset($_POST["update"])){
            $reduc = new Reduc_model();
            $reduc->code = $this->input->post('code');
            $reduc->pourcentage = $this->input->post('pourcentage');
            $reduc->restant = $this->input->post('restant');
            $reduc->id = $this->input->post('id');
            $reduc->update();
        }



        $data = array();
        $data["promos"] = $this->Reduc_model->getPromo();

        $this->load->helper('form');

        $this->load->view('templates/template', array('view'=>'admin/edit_promo', "data"=>$data));
    }

}