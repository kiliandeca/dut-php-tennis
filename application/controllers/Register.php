<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/01/2018
 * Time: 20:05
 */

class Register extends CI_Controller
{
    public function index()
    {
        if(isset($this->session->logged)){
            redirect('./', 'refresh');
        }

        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        $this->load->database();

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[CPOA_USER.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[124]');
        $this->form_validation->set_rules('passwordconf', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/template', array('view'=>'register'));
        }
        else
        {
            //Success
            $this->load->model('User_model', '', TRUE);

            $user = new User_model();
            $user->email = $this->input->post('email');
            $user->password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $user->insert();
            $this->session->set_userdata(array("logged" => true,"id" => $user->id, "email" => $user->email, "rank" => $user->rank));

            $this->load->view('templates/template', array('view'=>'loginsuccess'));
        }
    }

}