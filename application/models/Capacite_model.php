<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 22:31
 */

class Capacite_model extends CI_Model
{
    public $id;
    public $court;
    public $block;
    public $capacite;
    public $categorie;

    private static $table = "CPOA_CAPACITE";

    public function getByCourtId($id){
        $query = $this->db->get_where(self::$table, "court = $id");
        $result= $query->result();
        return $result;
    }

    public function insert(){
        $this->db->insert(self::$table, $this);
        $this->id = $this->db->insert_id();
    }

    public function deleteById($id){
        $this->db->delete(self::$table, "id = $id");
    }

    public function update(){
        $this->db->update(self::$table, $this, array('id' => $this->id));
    }

}