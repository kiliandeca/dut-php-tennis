<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 19/01/2018
 * Time: 12:39
 */

class Cart_model extends CI_Model
{

    public $user;
    public $place;

    private static $table = "CPOA_CART";

    public function insert()
    {
        $this->db->insert(self::$table, $this);
    }

    public function getPlace($idmatch, $block, $numero){
        $this->db->where("idmatch", $idmatch);
        $this->db->where("idorder", 0);
        $this->db->where("block", $block);
        $this->db->where("numero", $numero);
        $query = $this->db->get(self::$table);
        $result = $query->custom_result_object("Place_model");
        return $result;
    }

    public function getCountByUserId($id){
        $query=$this->db->get_where('CPOA_CART', array("user" => $id));
        $result= $query->result();
        return sizeof($result);

    }

    public function getByUserId($id){
        $query=$this->db->get_where('CPOA_CART', array("user" => $id));
        $result= $query->result();

        $this->load->model('Place_model', '', TRUE);
        $listPlace = array();
        foreach ($result as $r){
            $listPlace[] = $this->Place_model->getById($r->place);
        }
        return $listPlace;

    }

    public function getTotalPriceByUserId($id){
        $places = $this->getByUserId($id);
        $total =0;
        foreach ($places as $p){
            $total += $p->prix;
        }
        return $total;
    }

    public function removeByUserId($id, $orderId){
        $places = $this->getByUserId($id);
        foreach ($places as $p){
            $this->db->delete(self::$table, array('place' => $p->id));

            $this->db->where("id", $p->id);
            $this->db->update("CPOA_PLACE", array("idorder"=>$orderId));
        }
    }

    public function removeByPlaceAndUserId($userid, $placeid){
        $this->db->delete(self::$table, array('place' => $placeid, "user" => $userid));
    }
}