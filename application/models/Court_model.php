<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 22:31
 */

class Court_model extends CI_Model
{
    public $id;
    public $nom;

    private static $table = "CPOA_COURT";

    public function getCourts(){
        $query = $this->db->get(self::$table);
        $result= $query->result();
        return $result;
    }

    public function getById($id){

        $query = $this->db->get_where(self::$table, "id=$id");
        $result= $query->result();
        if(isset($result[0])){
            return $result[0];
        }

        return null;
    }

}