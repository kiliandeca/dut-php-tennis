<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 00:28
 */

class Match_model extends CI_Model
{
    public $id;
    public $tournoi;
    public $court;
    public $date;
    public $type;
    public $label;
    public $prix_base;

    private static $table = "CPOA_MATCH_VENTE";

    public function get_matches(){
        $query = $this->db->get(self::$table);
        $result = $query->custom_result_object("Match_model");
        return $result;
    }

    public function getById($id){
        $query = $this->db->get_where(self::$table, array('id' => $id));
        $result = $query->custom_result_object("Match_model");

        if(isset($result[0])){
            return $result[0];
        }

        return null;
    }

    public function insert(){
        $query = $this->db->get_where("CPOA_MATCH", "id = $this->id");
        $result = ($query->result())[0];
        $this->tournoi = $result->tournoi;
        $this->court = $result->court;

        $query = $this->db->get_where("CPOA_TOURNOI", "id = $this->tournoi");
        $result = ($query->result())[0];

        $this->type = $result->type;

        $this->date = "0000-00-0 00:00:00"; //TO:DO Generer a partir des horraires de la BD

        $this->db->insert(self::$table, $this);
        
        $this->load->model("Court_model", "", TRUE);
        $this->generatePlaces($this->id, ($this->Court_model->getById($this->court))->nom);
    }

    public function getPlacesLibresCount(){
        $this->db->select('*');
        $this->db->from('CPOA_PLACE');
        $this->db->where("idmatch = $this->id and idorder = 0");
        $query = $this->db->get();
        $result= $query->result();
        return sizeof($result);
    }

    public function getCourtName(){
        $query = $this->db->get_where("CPOA_COURT", array('id' => $this->court));
        $result= $query->result();
        if(isset($result[0])){
            return $result[0]->nom;
        }

        return null;
    }

    public function getNotInSale(){

        $listId = array();
        foreach ($this->get_matches() as $match) {
            $listId[] = $match->id;
        }


        $this->db->select('*');
        $this->db->from('CPOA_MATCH');
        $this->db->where_not_in('id', $listId);
        $query = $this->db->get();
        $result= $query->result();
        return($result);

    }

    public function generatePlaces($idMatch, $labelCourt){
        $this->load->model("Capacite_model", "", TRUE);
        $capacite = $this->Capacite_model->getByCourtId($this->court);

        $place = array();
        $place["idorder"] = 0;
        $place["idmatch"] = $idMatch;
        $place["labelCourt"] = $labelCourt;

        $place["prix"] = $this->prix_base; //TO:DO Calcule en fonction de la categorie de place

        foreach ($capacite as $block){
            $place["block"] = $block->block;
            foreach (range(1,$block->capacite) as $numero) {
                $place["numero"] = $numero;
                $this->db->insert("CPOA_PLACE", $place);
            }

        }
    }

}