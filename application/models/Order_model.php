<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 19/01/2018
 * Time: 15:33
 */

class Order_model extends CI_Model
{

    private static $table = "CPOA_ORDER";

    public $id;
    public $iduser;
    public $date;
    public $prix_total;

    public function insert(){
        $this->db->insert(self::$table, $this);
        $this->id = $this->db->insert_id();
    }

    public function getById($id){
        $querry = $this->db->get_where(self::$table, array("id"=> $id));
        $result = $querry->custom_result_object("Order_model");
        return $result[0];
    }

    public function getByUser($id){
        $querry = $this->db->get_where(self::$table, array("iduser"=> $id));
        $result = $querry->custom_result_object("Order_model");
        return $result;
    }

}