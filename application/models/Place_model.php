<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 19/01/2018
 * Time: 12:39
 */

class Place_model extends CI_Model
{

    public $id;
    public $idmatch;
    public $idorder;
    public $prix;
    public $block;
    public $numero;
    public $labelCourt;

    private static $table = "CPOA_PLACE";


    public function getBlocksLibres($idmatch)
    {
        $this->db->distinct();
        $this->db->select('block');
        $this->db->where("idmatch = $idmatch and idorder = 0");
        $query = $this->db->get(self::$table);
        $result = $query->result();

        return $result;
    }

    public function getPlacesLibresByBlock($idmatch, $block)
    {
        $this->db->select('numero');
        $this->db->where("idmatch", $idmatch);
        $this->db->where("idorder", 0);
        $this->db->where("block", $block);
        $query = $this->db->get(self::$table);
        $result = $query->result();

        return $result;

    }

    public function getPlace($idmatch, $block, $numero){
        $this->db->where("idmatch", $idmatch);
        $this->db->where("idorder", 0);
        $this->db->where("block", $block);
        $this->db->where("numero", $numero);
        $query = $this->db->get(self::$table);
        $result = $query->custom_result_object("Place_model");
        return $result;
    }

    public function getById($id){
        $querry = $this->db->get_where(self::$table, "id = $id");
        $result = $querry->custom_result_object("Place_model");
        return $result[0];
    }

    public function getByOrder($id){
        $querry = $this->db->get_where(self::$table, array("idorder"=> $id));
        $result = $querry->custom_result_object("Place_model");
        return $result;

    }

}