<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 22/01/2018
 * Time: 22:04
 */

class Reduc_model extends CI_Model
{

    public $id;
    public $code;
    public $pourcentage;
    public $restant;

    private static $table = "CPOA_REDUC";

    public function getByCode($code){
        $query = $this->db->get_where(self::$table, array("code"=>$code));
        return $query->custom_result_object("Reduc_model");
    }

    public function useCode(){
        $this->restant = $this->restant -1;
        $this->db->update(self::$table, $this, array('id' => $this->id));

    }

    public function getPromo(){
        $query = $this->db->get(self::$table);
        $result= $query->result();
        return $result;
    }

    public function insert(){
        $this->db->insert(self::$table, $this);
        $this->id = $this->db->insert_id();
    }

    public function update(){
        $this->db->update(self::$table, $this, array('id' => $this->id));
    }

    public function deleteById($id){
        $this->db->delete(self::$table, "id = $id");
    }

}