<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/01/2018
 * Time: 18:57
 */

class User_model extends CI_Model
{

    public $id;
    public $email;
    public $password;
    public $rank=0;

    private static $table = "CPOA_USER";

    public function get_by_email($arg){
        $query = $this->db->get_where(self::$table, array('email' => $arg));

        $result = $query->custom_result_object("User_model");

        if(isset($result[0])){
            return $result[0];
        }

        return null;
    }

    public function insert(){
        $this->db->insert(self::$table, $this);
        $this->id = $this->db->insert_id();
    }

}