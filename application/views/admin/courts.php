<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 22:18
 */
?>


<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">

            <h1>Selectionez un court à modifier</h1>

                <?php echo validation_errors(); ?>
                <?php echo form_open('admin/courts'); ?>
                <div class="form-group mb-2">
                    <select class="form-control" name="court">

                        <?php foreach($courts as $court){ ?>

                            <option value="<?=$court->id?>"><?=$court->nom?></option>



                        <?php } ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mb-2">Valider</button>
            </form>

        </div>
    </div>
</div>