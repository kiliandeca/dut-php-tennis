<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 22:18
 */
?>


<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">

            <h1>Court: <?=$court->nom?></h1>
            <?php echo form_open('admin/courts/'.$court->id); ?>

            <?php if(!isset($_POST["add"])){ ?>
                <button type="submit" class="btn btn-primary" name="add">Ajouter</button>

                <?php }?>



            <table class="table table-dark table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">Block</th>
                    <th scope="col">Capacite</th>
                    <th scope="col">Categorie</th>
                    <th scope="col"></th>
                </tr>
                </thead>

                <?php if(isset($_POST["add"])){ ?>
                    <td><input class="form-control" type="text" placeholder="Block label" name="block"></td>
                    <td><input class="form-control" type="text" placeholder="Capacitée" name="capacite"></td>
                    <td><input class="form-control" type="text" placeholder="Categorie" name="categorie"></td>
                    <td><button type="submit" class="btn btn-success" name="save">Valider</button></td>

                <?php }?>
                <?php foreach($blocks as $block){ ?>
                    <tr>

                        <?php if(!isset($_POST["edit"]) or $_POST["edit"]!= $block->id){?>
                            <th scope="row"><?=$block->block?></th>
                            <td><?=$block->capacite?></td>
                            <td><?=$block->categorie?></td>
                            <td>

                                <button type="submit" class="btn btn-warning" name="edit" value="<?=$block->id?>">edit</button>
                                <button type="submit" class="btn btn-danger" name="delete" value="<?=$block->id?>">X</button>
                            </td>
                        <?php } else { ?>
                            <td><input class="form-control" type="text" placeholder="Block label" value="<?=$block->block?>" name="block"></td>
                            <td><input class="form-control" type="text" placeholder="Capacitée" value="<?=$block->capacite?>" name="capacite"></td>
                            <td><input class="form-control" type="text" placeholder="Categorie" value="<?=$block->categorie?>" name="categorie"></td>
                            <input name="id" type="hidden" value="<?=$block->id?>">
                            <td><button type="submit" class="btn btn-success" name="update">Enregistrer</button></td>
                        <?php } ?>
                    </tr>
                </form>

            <?php } ?>

                </tbody>
            </table>


        </div>
    </div>
</div>