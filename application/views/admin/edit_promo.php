<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 22:18
 */
?>


<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">

            <h1>Codes Promo:</h1>
            <?php echo form_open('admin/promo/'); ?>

            <?php if(!isset($_POST["add"])){ ?>
                <button type="submit" class="btn btn-primary" name="add">Ajouter</button>

                <?php }?>



            <table class="table table-dark table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Code</th>
                    <th scope="col">Pourcentage</th>
                    <th scope="col">Restant</th>
                </tr>
                </thead>

                <?php if(isset($_POST["add"])){ ?>
                    <td><input class="form-control" type="text" placeholder="code" name="code"></td>
                    <td><input class="form-control" type="text" placeholder="pourcentage" name="pourcentage"></td>
                    <td><input class="form-control" type="text" placeholder="restant" name="restant"></td>
                    <td><button type="submit" class="btn btn-success" name="save">Valider</button></td>

                <?php }?>
                <?php foreach($promos as $promo){ ?>
                    <tr>

                        <?php if(!isset($_POST["edit"]) or $_POST["edit"]!= $promo->id){?>
                            <th scope="row"><?=$promo->id?></th>
                            <td><?=$promo->code?></td>
                            <td><?=$promo->pourcentage?></td>
                            <td><?=$promo->restant?></td>
                            <td>

                                <button type="submit" class="btn btn-warning" name="edit" value="<?=$promo->id?>">edit</button>
                                <button type="submit" class="btn btn-danger" name="delete" value="<?=$promo->id?>">X</button>
                            </td>
                        <?php } else { ?>
                            <td><input class="form-control" type="text" placeholder="code" value="<?=$promo->code?>" name="code"></td>
                            <td><input class="form-control" type="text" placeholder="pourcentage" value="<?=$promo->pourcentage?>" name="pourcentage"></td>
                            <td><input class="form-control" type="text" placeholder="restant" value="<?=$promo->restant?>" name="restant"></td>
                            <input name="id" type="hidden" value="<?=$promo->id?>">
                            <td><button type="submit" class="btn btn-success" name="update">Enregistrer</button></td>
                        <?php } ?>
                    </tr>
                </form>

            <?php } ?>

                </tbody>
            </table>


        </div>
    </div>
</div>