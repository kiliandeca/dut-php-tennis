<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 00:01
 */
?>




<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">
            <h1>Liste des Matchs:</h1>
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Date</th>
                        <th scope="col">Places restantes</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($matchs as $match){ ?>
                    <tr>
                        <th scope="row"><?=$match->id?></th>
                        <td><?=$match->label?> <span class="badge badge-pill badge-<?= $match->type == 1 ? "danger":"success" ?>"><?= $match->type == 1 ? "Simple":"Double" ?></span></td>
                        <td><?=$match->date?></td>
                        <td><?=$match->getPlacesLibresCount()?></td>
                        <td><a class="btn btn-<?=$match->getPlacesLibresCount()>0 ? "primary":"dark disabled" ?>" href="matchs/<?=$match->id?>" role="button"><?=$match->getPlacesLibresCount()>0 ? "Acheter des places":"Sold out"?></a></td>
                    </tr>
                        </a>
                    <?php } ?>

                </tbody>
            </table>

        </div>
    </div>
</div>