<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 00:03
 */
?>

<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">
            <h1>Match: <?=$match->label?> <span class="badge badge-pill badge-<?= $match->type == 1 ? "danger":"success" ?>"><?= $match->type == 1 ? "Simple":"Double" ?></span></h1>
            <h2>Date: <?=$match->date?></h2>
            <h2>Court: <?=$match->getCourtName()?></h2>
            <h4>Places restantes: <?=$match->getPlacesLibresCount()?></h4>


            <form method="get">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Example select</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="block" onchange="this.form.submit()">
                        <option value="">Blocks dispo:</option>
                        <?php foreach ($blocks as $block){ ?>

                           <option <?=$block->block==$this->input->get("block") ? "selected=\"selected\"":"" ?>><?=$block->block?></option>
                        <?php } ?>
                    </select>
                </div>


                <?php if($this->input->get("block")!=null) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlSelect2">Example select</label>
                        <select class="form-control" id="exampleFormControlSelect2" name="num" onchange="this.form.submit()">
                            <option value="">Places dispo:</option>
                            <?php foreach ($numeros as $numero){ ?>
                                <option <?=$numero->numero==$this->input->get("num") ? "selected=\"selected\"":"" ?>><?=$numero->numero?></option>
                            <?php } ?>
                        </select>
                    </div>

                <?php } ?>

            </form>

            <?php if(isset($place)){?>
            <form method="post" action="<?=base_url("/matchs/".$match->id)?>" class="form-inline">
                <div class="form-group mb-2" style="padding-right: 20px">
                    <h1>Prix: <?=$place->prix?>  </h1>
                </div>

                <input name="id" type="hidden" value="<?=$place->id?>">


                <button type="submit" class="btn btn-primary mb-2" name="add" value="1">Ajouter au panier</button>

            <?php } ?>




        </div>
    </div>
</div>