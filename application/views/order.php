<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 00:01
 */
?>




<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">
            <h1>Order:</h1>
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Match</th>
                        <th scope="col">Court</th>
                        <th scope="col">Place</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($places as $place){ ?>
                    <tr>
                        <th scope="row"><?=$place->idmatch?></th>
                        <td><?=$place->labelCourt?></td>
                        <td><?=$place->block." ".$place->numero?></td>
                        <td></td>
                    </tr>
                        </a>
                    <?php } ?>



                </tbody>
            </table>

        </div>
    </div>
</div>