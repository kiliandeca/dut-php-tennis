<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 00:01
 */
?>




<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">
            <h1>Panier:</h1>
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Date</th>
                        <th scope="col">Total</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($orders as $order){ ?>
                    <tr>
                        <th scope="row"><?=$order->id?></th>
                        <td><?=$order->date?></td>
                        <td><?=$order->prix_total?></td>
                        <td><a class="btn btn-primary" href="<?=base_url("orders/".$order->id)?>" role="button">Détails</a></td>
                    </tr>
                        </a>
                    <?php } ?>



                </tbody>
            </table>
        </div>
    </div>
</div>