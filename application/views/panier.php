<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 18/01/2018
 * Time: 00:01
 */
?>




<div class="row justify-content-center">
    <div class="col-6 align-self-center">
        <div class="card card-body bg-dark text-white" style="margin-top: 100px">
            <h1>Panier:</h1>

            <?php if($places!=null){?>
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Match</th>
                        <th scope="col">Court</th>
                        <th scope="col">Place</th>
                        <th scope="col">Prix</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($places as $place){ ?>
                    <tr>
                        <th scope="row"><?=$place->idmatch?></th>
                        <td><?=$place->labelCourt?></td>
                        <td><?=$place->block." ".$place->numero?></td>
                        <td><?=$place->prix?></td>
                        <td>
                            <form action="<?=base_url("panier")?>" method="post">
                                <button type="submit" class="btn btn-danger" name="delete" value="<?=$place->id?>">X</button>
                            </form>
                        </td>
                    </tr>
                    <?php } if(isset($promo)){?>

                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Code Promo</th>
                        <th scope="col"><?= $promo->pourcentage?>%</th>
                        <th scope="col">-<?=number_format($prixTotal*($promo->pourcentage/100),2)?>€</th>
                        <th scope="col"></th>

                    </tr>

                    <?php $prixTotal = $prixTotal-number_format($prixTotal*($promo->pourcentage/100),2);
                    } ?>

                </tbody>
            </table>

                <?php if(!isset($promo)){ ?>
                <form method="post" action="<?=base_url("/panier")?>" class="form-inline">
                    <div class="form-group mb-2" style="padding-right: 20px">
                        <input type="text" class="form-control" name="promo" placeholder="Code Promo" size="10" />
                    </div>
                    <button type="submit" class="btn btn-primary mb-2" name="" value="">Ajouter code promo</button>

                </form>
                <?php } ?>


                <h1>Total: <?=$prixTotal?>€</h1>

            <form action="<?=base_url("paiement")?>" method="post">
                <?php if(isset($promo)){ ?>
                    <input name="promo" type="hidden" value="<?=$promo->code?>">
                <?php } ?>

                <button type="submit" class="btn btn-success">Acheter</button>
            </form>
            <?php }else{ ?>

                <h2>Votre panier est vide !</h2>

            <?php } ?>
        </div>
    </div>
</div>