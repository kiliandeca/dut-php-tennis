<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/01/2018
 * Time: 21:02
 */
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="<?=base_url()?>">GPTL</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item <?php if($this->uri->segment(1)==""){echo 'class="active"';}?>">
                <a class="nav-link" href="<?=base_url()?>"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('matchs')?>">Matchs</a>
            </li>


        </ul>
        <ul class="navbar-nav navbar-right ml-auto">

            <?php if(!isset($this->session->logged)){ ?>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('login')?>">Connexion</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('register')?>">Inscription</a>
            </li>
            <?php } else { ?>

                <li class="nav-item">
                    <?php
                        $count = $this->session->cart != null ? $this->session->cart : 0;
                    ?>
                    <a class="nav-link" href="<?=base_url('panier')?>">Panier <span class="badge badge-pill badge-<?=$count > 0 ? "info":"secondary"?>"><?=$count ?></span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url('orders')?>">Orders</a>
                </li>

                <?php if(isset($this->session->rank)and $this->session->rank>0){?>

                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url('admin')?>">Panel admin</a>
                </li>

                <?php } ?>

                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url('logout')?>">Deconnexion</a>
                </li>

            <?php } ?>

        </ul>



    </div>
</nav>