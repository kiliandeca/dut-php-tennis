<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/01/2018
 * Time: 21:10
 */

$this->load->view('templates/header');
$this->load->view('templates/menu');

?>
<div class="container-fluid bg">
<?php

if(!isset($data))$data = null;
$this->load->view($view, $data);